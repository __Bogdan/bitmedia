using System;

namespace MailService.Repo
{
    public enum Status
    {Sending = 0, Ok, Error}
    public class EmailDTO
    {
        public virtual Guid Guid {get; set;}
        public virtual Status Status {get; set;}
    }
}