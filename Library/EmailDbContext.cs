using Microsoft.EntityFrameworkCore;

namespace MailService.Repo
{
    public class EmailDbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<EmailDTO>().HasKey(e => e.Guid);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=./mydb.db");
        }

        public virtual DbSet<EmailDTO> Emails { get; set; }
    }
}