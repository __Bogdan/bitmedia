using System;
using System.Linq;

namespace MailService.Repo
{
    public class EmailRepo
    {
        public EmailRepo() { }

        public void Add(Guid id)
        {
            using (var ctx = new EmailDbContext())
            {
                ctx.Emails.Add(new EmailDTO { Guid = id, Status = Status.Sending });
                ctx.SaveChanges();
            }
        }

        public Status GetStatus(Guid id)
        {
            using (var ctx = new EmailDbContext())
                return ctx.Emails.FirstOrDefault(o => o.Guid == id).Status;
        }

        public void SetSuccess(Guid id)
        {
            SetStatus(id, Status.Ok);
        }

        public void SetError(Guid id)
        {
            SetStatus(id, Status.Error);
        }

        private void SetStatus(Guid id, Status status)
        {
            using (var ctx = new EmailDbContext())
            {
                ctx.Emails.FirstOrDefault(o => o.Guid == id)
                    .Status = status;
                ctx.SaveChanges();
            }
        }
    }
}