using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using MailService.Models;
using MailService.Repo;
using Nito.AsyncEx;

namespace MailService.Services
{
    public class EmailService
    {
        public EmailService(
            EmailRepo repo,
            IConfigurationService conf)
        {
            ReadConfig(conf);
            int connectionsNo = conf.GetConnectionsNumber();
            _connections = new LinkedList<SmtpClient>();
            _locks = new Dictionary<SmtpClient, AsyncMonitor>();
            connectionsNo = connectionsNo > 0 ? connectionsNo : 1;
#if DEBUG
            Console.WriteLine(connectionsNo);
#endif
            for (int i = 0; i < connectionsNo; i++)
            {
                CreateSmtpClient();
            }
            _currConnection = _connections.First;
            _repo = repo;
        }

        public async Task<SendResult> Send(Email email)
        {
            var guid = Guid.NewGuid();
            var client = _currConnection.Value;
            var semaphore = _locks[client];

            _repo.Add(guid);
            _currConnection = _currConnection.Next ?? _connections.First;

            MailMessage msg = new MailMessage(
                new MailAddress(email.FromAddress, email.FromName),
                new MailAddress(email.ToAddress, email.ToName));
            msg.Body = email.Body;
            msg.Subject = email.Subject;

            using (await semaphore.EnterAsync())
            {
#if DEBUG
                Console.WriteLine("Entered at {0}", DateTime.Now);
                // await Task.Delay(5000);
#endif
                client.SendAsync(msg, guid);
#if DEBUG
                Console.WriteLine("Released at {0}", DateTime.Now);
#endif
            }
            return new SendResult() { Id = guid };
        }

        private void SendCompletedCb(object sender, AsyncCompletedEventArgs e)
        {
            var mailId = (Guid)e.UserState;

            if (e.Error != null)
            {
                _repo.SetError(mailId);
            }
            else
            {
                _repo.SetSuccess(mailId);
            }
        }

        private void CreateSmtpClient()
        {
            if (string.IsNullOrEmpty(_domain))
                throw new ArgumentException();

            var conn = new SmtpClient()
            {
                Host = _domain,
                Port = _port,
                EnableSsl = _enableSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(_addr, _password),
                Timeout = _timeout
            };
            conn.SendCompleted += SendCompletedCb;

            _locks[conn] = new AsyncMonitor();
            _connections.AddLast(conn);
        }

        private void ReadConfig(IConfigurationService conf)
        {
            this._domain = conf.GetDomain();
            this._port = conf.GetPort();
            this._enableSsl = conf.GetEnableSsl();
            this._timeout = conf.GetTimeout();
            this._addr = conf.GetAddr();
            this._password = conf.GetPassword();

        }
        private string _domain;
        private int _port;
        private bool _enableSsl;
        private int _timeout;
        private string _addr;
        private string _password;
        private string _smtpDomain { get; set; }
        private LinkedListNode<SmtpClient> _currConnection { get; set; }
        private LinkedList<SmtpClient> _connections { get; set; }
        private IDictionary<SmtpClient, AsyncMonitor> _locks { get; set; }
        private EmailRepo _repo { get; set; }
    }
}
