namespace MailService.Services
{
    public interface IConfigurationService
    {
        string GetDomain();
        string GetAddr();
        string GetPassword();
        int GetPort();
        bool GetEnableSsl();
        int GetTimeout();
        int GetConnectionsNumber();
    }
}