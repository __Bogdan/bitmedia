using System;

namespace MailService.Services
{
    public class ConfigurationService : IConfigurationService
    {
        public string GetDomain()
        {
            return Environment.GetEnvironmentVariable("email_service_domain");
        }
        public string GetAddr()
        {
            return Environment.GetEnvironmentVariable("email_service_addr");

        }
        public string GetPassword()
        {
            return Environment.GetEnvironmentVariable("email_service_password");

        }
        public int GetPort()
        {
            if (int.TryParse(
                Environment.GetEnvironmentVariable("email_service_port"),
                out int port))
            {
                return port;
            }
            return 25;
        }

        public bool GetEnableSsl()
        {
            if (int.TryParse(
                Environment.GetEnvironmentVariable("email_service_enable_ssl"),
                out int enableSsl))
            {
                return enableSsl != 0;
            }
            return false;
        }

        public int GetTimeout()
        {
            if (int.TryParse(
                Environment.GetEnvironmentVariable("email_service_timeout"),
                out int timeout))
            {
                return timeout;
            }
            return 10000;
        }

        public int GetConnectionsNumber()
        {
            if (int.TryParse(
                Environment.GetEnvironmentVariable("email_service_pool_size"),
                out int connNo))
            {
                return connNo;
            }
            return 1;
        }

    }
}