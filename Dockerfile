FROM mcr.microsoft.com/dotnet/core/sdk:3.1

COPY ./run.sh $HOME/run.sh

ENV email_service_domain="localhost"
ENV email_service_addr="bgdn2014@gmail.com"
ENV email_service_password=""
ENV email_service_port="25"
ENV email_service_enable_ssl="0"
ENV email_service_pool_size="3"

ENV PATH="$PATH:/root/.dotnet/tools"
ENV ASPNETCORE_URLS="http://0.0.0.0:3000"

COPY ./nuget.config $HOME/nuget.config
COPY ./Library $HOME/Library
COPY ./MailService $HOME/MailService

EXPOSE 3000/tcp
ENTRYPOINT ["./run.sh"] 
