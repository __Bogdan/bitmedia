using GraphQL.Types;

namespace MailService
{
    public class StatusViewModelType: ObjectGraphType<StatusViewModel>
    {
        public StatusViewModelType()
        {
            Name = "StatusViewModel";
            
            Field(x => x.Guid).Description("Id of email");
            Field(x => x.Status).Description("Status of email delivery");
        }
    }
}