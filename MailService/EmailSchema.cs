using GraphQL.Types;
using GraphQL.Utilities;
using System;

namespace MailService
{
    public class EmailSchema : Schema
    {
        public EmailSchema(IServiceProvider provider)
            : base(provider)
        {
            Query = provider.GetRequiredService<EmailStatusQuery>();
            Mutation = provider.GetRequiredService<EmailMutation>();
        }
    }
}
