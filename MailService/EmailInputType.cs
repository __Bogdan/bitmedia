using GraphQL.Types;
using MailService.Models;

namespace MailService
{
    public class EmailInputType : InputObjectGraphType<Email>
    {
        public EmailInputType()
        {
            Name = "EmailInput";
            Field(x => x.FromAddress);
            Field(x => x.FromName, nullable: true);
            Field(x => x.ToAddress);
            Field(x => x.ToName, nullable: true);
            Field(x => x.Subject);
            Field(x => x.Body);
        }
    }
}
