using System;

namespace MailService
{
    public class StatusViewModel
    {
        public virtual Guid Guid { get; set; }
        public virtual string Status { get; set; }
    }
}