using System;
using GraphQL;
using GraphQL.Types;
using MailService.Repo;

namespace MailService
{
    public class EmailStatusQuery : ObjectGraphType
    {
        public EmailStatusQuery(EmailRepo ctx)
        {
            Name = "emailStatus";

            Field<StatusViewModelType>(
                "email",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "id of email" }
                ),
                resolve: context => new StatusViewModel()
                {
                    Guid = context.GetArgument<Guid>("id"),
                    Status = ctx.GetStatus(context.GetArgument<Guid>("id")).ToString()
                }
                
            );
        }
    }
}

